const dato = document.getElementById('personaje');
dato.addEventListener('change',function(){
    const personaje = document.getElementById('personaje').value
    getAllRequest(personaje)
})


function getAllRequest(data: any)
{
   axios.get(`https://rickandmortyapi.com/api/character?name=${data}`)
     .then(function(response: { data: { results: any; }; }) {
        const resultados = response.data.results
        const card = document.getElementById('card');
        for (let index = 0; index < resultados.length; index++) {
            const elemento = resultados[index];
            console.log(elemento)

            const h1 = document.createElement('h1')
            const p = document.createElement('p')
            const imagen = document.createElement('img')
            h1.textContent = elemento.name
            p.textContent = elemento.status
            imagen.setAttribute('src',elemento.image)
            card.appendChild(h1)
            card.appendChild(p)
            card.appendChild(imagen)
        }
    })
}